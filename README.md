1. Собираем проект: mvn package
2. Настраиваем БД: 
   DB: postgres
   username: postgres
   password: postgres
3. Запускаем артефакт target/book-library-0.0.1-SNAPSHOT.jar
4. Документация Swagger: http://localhost:8081/api/swagger-ui.html