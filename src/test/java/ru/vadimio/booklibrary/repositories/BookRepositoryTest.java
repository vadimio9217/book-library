package ru.vadimio.booklibrary.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import ru.vadimio.booklibrary.entities.Author;
import ru.vadimio.booklibrary.entities.Book;

@DataJpaTest
@ActiveProfiles("test")
class BookRepositoryTest {

  private static final String SEARCH_KEY = "Book1";
  private static final Short YEAR = 2020;
  private static final Long AUTHORS_NUM = 2L;
  private static final PageRequest PAGE_REQUEST = PageRequest.of(0, 20);
  private static final Specification<Book> SPEC_FILTER_BY_SEARCH_KEY = BookSpecification
      .filterBySearchKey(SEARCH_KEY);
  private static final Specification<Book> SPEC_FILTER_BY_YEAR = BookSpecification
      .filterByYear(YEAR);
  private static final Specification<Book> SPEC_FILTER_BY_AUTHOR_NUM = BookSpecification
      .filterByAuthorNum(AUTHORS_NUM);

  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private AuthorRepository authorRepository;

  @BeforeEach
  void init() {
    Author author1 = Author.builder()
        .id(UUID.randomUUID())
        .name("Author1")
        .build();
    Author author2 = Author.builder()
        .id(UUID.randomUUID())
        .name("Author2")
        .build();
    Book book1 = Book.builder()
        .id(UUID.randomUUID())
        .title("Book1")
        .year((short) 2020)
        .build();
    Book book2 = Book.builder()
        .id(UUID.randomUUID())
        .authors(List.of(author1, author2))
        .title("Book2")
        .build();

    authorRepository.saveAll(List.of(author1, author2));
    bookRepository.saveAll(List.of(book1, book2));
    author1.setBook(book2);
    author2.setBook(book2);
    authorRepository.saveAll(List.of(author1, author2));
  }

  @Test
  void shouldFindAll() {
    Page<Book> books = bookRepository.findAll(PAGE_REQUEST);
    assertEquals(2, books.getTotalElements());
  }

  @Test
  void shouldFindBySearchKey() {
    Page<Book> books = bookRepository.findAll(SPEC_FILTER_BY_SEARCH_KEY, PAGE_REQUEST);
    assertEquals(1, books.getTotalElements());
    assertEquals((short) 2020, books.getContent().get(0).getYear());
  }

  @Test
  void shouldFindByYear() {
    Page<Book> books = bookRepository.findAll(SPEC_FILTER_BY_YEAR, PAGE_REQUEST);
    assertEquals(1, books.getTotalElements());
    assertEquals("Book1", books.getContent().get(0).getTitle());
  }

  @Test
  void shouldFindByAuthorsNum() {

    Page<Book> books = bookRepository.findAll(SPEC_FILTER_BY_AUTHOR_NUM, PAGE_REQUEST);
    assertEquals(1, books.getTotalElements());
    assertEquals("Book2", books.getContent().get(0).getTitle());
  }

}
