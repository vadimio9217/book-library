package ru.vadimio.booklibrary.services;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import ru.vadimio.booklibrary.exceptions.ValidationException;

@TestInstance(Lifecycle.PER_CLASS)
class ValidationServiceTest {

  private static final String SEARCH_KEY_SIZE_EXCEPTION = "Size of search key must be lesser than 255 characters";
  private static final String SEARCH_KEY_REGEX_EXCEPTION = "Characters % $ @ ! ? * are not supported in search key";
  private static final String YEAR_EXCEPTION = "Year must be in range from 1800 to 2021";
  private static final String AUTHORS_NUM_EXCEPTION = "Number of authors must be in range from 0 to 100";
  private static final String SEARCH_KEY = "hasdksaldsasadaskhsdajsakl"
      + "dsadalksdhsaklashjskahsadashkasdlkasdhashasl"
      + "kdakashksadkhksjsahsadaskashsadhasjsahaskhsa"
      + "djkashsahjsahjassahdksadhkksajhsajkkjsadsdfd"
      + "asdsdasdasdasasafdsfdfsdfsdfsdsffsdfdsdsffss";
  private static final String REGEX = "hhsda% kllk";
  private static final Short YEAR = 100;
  private static final Long AUTHORS_NUM = -1L;
  private ValidationService validationService;

  @BeforeAll
  void init() {
    validationService = new ValidationService();
  }

  @Test
  void shouldThrowKeySizeException() {
    try {
      validationService.validate(SEARCH_KEY, null, null);
    } catch (ValidationException e) {
      assertTrue(e.getMessage().contains(SEARCH_KEY_SIZE_EXCEPTION));
    }
  }

  @Test
  void shouldThrowRegexException() {
    try {
      validationService.validate(REGEX, null, null);
    } catch (ValidationException e) {
      assertTrue(e.getMessage().contains(SEARCH_KEY_REGEX_EXCEPTION));
    }
  }

  @Test
  void shouldThrowYearException() {
    try {
      validationService.validate(null, YEAR, null);
    } catch (ValidationException e) {
      assertTrue(e.getMessage().contains(YEAR_EXCEPTION));
    }
  }

  @Test
  void shouldThrowAuthorsNumException() {
    try {
      validationService.validate(null, null, AUTHORS_NUM);
    } catch (ValidationException e) {
      assertTrue(e.getMessage().contains(AUTHORS_NUM_EXCEPTION));
    }
  }
}
