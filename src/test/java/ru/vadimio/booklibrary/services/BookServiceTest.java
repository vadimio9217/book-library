package ru.vadimio.booklibrary.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import ru.vadimio.booklibrary.entities.Author;
import ru.vadimio.booklibrary.entities.Book;
import ru.vadimio.booklibrary.models.AuthorDto;
import ru.vadimio.booklibrary.models.BookDto;
import ru.vadimio.booklibrary.repositories.BookRepository;
import ru.vadimio.booklibrary.repositories.BookSpecification;

@SpringBootTest(classes = {
    BookService.class,
    BookRepository.class
})
class BookServiceTest {

  @Autowired
  private BookService bookService;

  @MockBean
  private BookRepository bookRepository;

  @Test
  void shouldReturnPageOfBookDto() {
    AuthorDto authorDto = AuthorDto.builder()
        .id(UUID.randomUUID())
        .name("Author")
        .build();
    BookDto bookDto = BookDto.builder()
        .id(UUID.randomUUID())
        .title("Book")
        .authors(Collections.singletonList(authorDto))
        .build();
    Author author = Author.builder()
        .id(authorDto.getId())
        .name(authorDto.getName())
        .build();
    Book book = Book.builder()
        .id(bookDto.getId())
        .title(bookDto.getTitle())
        .authors(Collections.singletonList(author))
        .build();
    Specification<Book> spec = BookSpecification.trueLiteral();
    Pageable page = PageRequest.of(0, 20, Sort.by("title"));
    when(bookRepository.findAll(spec, page))
        .thenReturn((new PageImpl<>(Collections.singletonList(book))));
    Page<BookDto> result = bookService.findByFilters(null, null, null);
    assertEquals(authorDto.getId(), result.getContent().get(0).getAuthors().get(0).getId());
    assertEquals(bookDto.getId(), result.getContent().get(0).getId());
  }
}
