package ru.vadimio.booklibrary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Objects;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import ru.vadimio.booklibrary.entities.Book;
import ru.vadimio.booklibrary.models.BookDto;
import ru.vadimio.booklibrary.repositories.BookRepository;
import ru.vadimio.booklibrary.utils.RestResponsePage;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class AllLayersTestIT {

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private BookRepository bookRepository;

  @Test
  void shouldAllLayersWork() {
    Book book = Book.builder()
        .id(UUID.randomUUID())
        .title("Book")
        .build();
    bookRepository.save(book);
    RestResponsePage<BookDto> response = restTemplate
        .getForObject("/v1/books", RestResponsePage.class);
    assertEquals(1, Objects.requireNonNull(response.getTotalElements()));
  }
}
