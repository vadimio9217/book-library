package ru.vadimio.booklibrary.controllers;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.Objects;
import java.util.UUID;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.vadimio.booklibrary.models.BookDto;
import ru.vadimio.booklibrary.services.BookService;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest {

  private Page<BookDto> page;
  private BookDto bookDto;

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private BookService bookService;

  @BeforeEach
  void init() {
    bookDto = BookDto.builder()
        .id(UUID.randomUUID())
        .title("Book")
        .build();
    page = new PageImpl<>(Collections.singletonList(bookDto),
        PageRequest.of(0, 20),
        Collections.singletonList(bookDto).size());
  }

  @Test
  @SneakyThrows
  void shouldReturnPageWithBookDto() {

    when(bookService.findByFilters(any(), any(), any())).thenReturn(page);
    mockMvc.perform(get("/v1/books")
        .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(bookDto.getId().toString())))
        .andExpect(content().string(containsString(bookDto.getTitle())));
  }

  @Test
  @SneakyThrows
  void shouldThrowException() {
    Objects.requireNonNull(mockMvc.perform(get("/v1/books")
        .param("year", "20200")
        .accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest())
        .andReturn().getResolvedException()).getMessage();
    ;
  }
}
