package ru.vadimio.booklibrary.repositories;

import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import ru.vadimio.booklibrary.entities.Book;

public final class BookSpecification {

  private BookSpecification() {
  }

  public static Specification<Book> trueLiteral() {
    return (root, query, builder) -> builder.isTrue(builder.literal(true));
  }

  public static Specification<Book> filterBySearchKey(String searchKey) {
    return (root, query, builder) -> builder.or(
        builder.like(root.get("title"), "%" + searchKey + "%"),
        builder.like(leftJoinTables(root).get("name"), "%" + searchKey + "%")
    );
  }

  public static Specification<Book> filterByYear(Short year) {
    return (root, query, builder) ->
        builder.equal(root.get("year"), year);
  }

  public static Specification<Book> filterByAuthorNum(Long authorNum) {
    return (root, query, builder) -> {
      query.groupBy(root.get("id"))
          .having(builder.equal(builder.count(leftJoinTables(root).get("name")), authorNum));
      return trueLiteral().toPredicate(root, query, builder);
    };
  }

  private static Path<Book> leftJoinTables(Root<Book> root) {
    return root.join("authors", JoinType.LEFT);
  }

}
