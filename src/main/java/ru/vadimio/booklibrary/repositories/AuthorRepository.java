package ru.vadimio.booklibrary.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vadimio.booklibrary.entities.Author;

@Repository
public interface AuthorRepository extends CrudRepository<Author, Integer> {

}
