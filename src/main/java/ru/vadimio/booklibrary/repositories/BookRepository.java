package ru.vadimio.booklibrary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.vadimio.booklibrary.entities.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer>,
    JpaSpecificationExecutor<Book> {

}
