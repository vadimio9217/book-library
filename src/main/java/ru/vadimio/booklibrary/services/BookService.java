package ru.vadimio.booklibrary.services;

import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.vadimio.booklibrary.entities.Book;
import ru.vadimio.booklibrary.models.AuthorDto;
import ru.vadimio.booklibrary.models.BookDto;
import ru.vadimio.booklibrary.repositories.BookRepository;
import ru.vadimio.booklibrary.repositories.BookSpecification;

@Service
@RequiredArgsConstructor
public class BookService {

  private final BookRepository bookRepository;

  public Page<BookDto> findByFilters(String searchKey, Short year, Long authorsNum) {
    Specification<Book> spec = BookSpecification.trueLiteral();
    if (searchKey != null) {
      spec = spec.and(BookSpecification.filterBySearchKey(searchKey));
    }
    if (year != null && spec != null) {
      spec = spec.and(BookSpecification.filterByYear(year));
    }
    if (authorsNum != null && spec != null) {
      spec = spec.and(BookSpecification.filterByAuthorNum(authorsNum));
    }
    Page<Book> entities = bookRepository.findAll(spec, PageRequest.of(0, 20, Sort.by("title")));
    return new PageImpl<>(entities.getContent()
        .stream()
        .map(entity -> BookDto.builder()
            .id(entity.getId())
            .title(entity.getTitle())
            .year(entity.getYear())
            .authors(entity.getAuthors().stream()
                .map(author -> AuthorDto.builder()
                    .id(author.getId())
                    .name(author.getName())
                    .build())
                .collect(Collectors.toList()))
            .build()
        ).collect(Collectors.toList()), entities.getPageable(), entities.getTotalElements());
  }
}
