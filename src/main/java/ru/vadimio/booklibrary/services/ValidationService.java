package ru.vadimio.booklibrary.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.vadimio.booklibrary.exceptions.ValidationException;

@Slf4j
@Service
public class ValidationService {

  private static final String SEARCH_KEY_SIZE_EXCEPTION = "Size of search key must be lesser than 255 characters";
  private static final String SEARCH_KEY_REGEX_EXCEPTION = "Characters % $ @ ! ? * are not supported in search key";
  private static final String YEAR_EXCEPTION = "Year must be in range from 1800 to 2021";
  private static final String AUTHORS_NUM_EXCEPTION = "Number of authors must be in range from 0 to 100";

  public void validate(String searchKey, Short year, Long authorsNum)
      throws ValidationException {

    if (searchKey != null) {
      if (searchKey.length() > 255) {
        log.error(SEARCH_KEY_SIZE_EXCEPTION);
        throw new ValidationException(SEARCH_KEY_SIZE_EXCEPTION);
      }
      if (searchKey.matches("^[%$@!?*]+")) {
        log.error(SEARCH_KEY_REGEX_EXCEPTION);
        throw new ValidationException(SEARCH_KEY_REGEX_EXCEPTION);
      }
    }
    if (year != null && (year < 1800 || year > 2021)) {
      log.error(YEAR_EXCEPTION);
      throw new ValidationException(YEAR_EXCEPTION);
    }
    if (authorsNum != null && (authorsNum < 0 || authorsNum > 100)) {
      log.error(AUTHORS_NUM_EXCEPTION);
      throw new ValidationException(AUTHORS_NUM_EXCEPTION);
    }
  }

}
