package ru.vadimio.booklibrary.entities;

import java.util.List;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "books")
public class Book {

  @Id
  @Type(type = "pg-uuid")
  private UUID id;
  private String title;
  private Short year;
  @OneToMany(mappedBy = "book")
  private List<Author> authors;
}
