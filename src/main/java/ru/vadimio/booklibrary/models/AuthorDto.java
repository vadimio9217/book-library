package ru.vadimio.booklibrary.models;

import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDto {

  @ApiModelProperty(required = true, value = "UUID of author")
  private UUID id;
  @ApiModelProperty(required = true, value ="Author name")
  private String name;
}
