package ru.vadimio.booklibrary.models;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {

  @ApiModelProperty(required = true, value = "UUID of book")
  private UUID id;
  @ApiModelProperty(required = true, value = "Book title")
  private String title;
  @ApiModelProperty("Year of creation")
  private Short year;
  @ApiModelProperty("Book authors")
  private List<AuthorDto> authors;
}
