package ru.vadimio.booklibrary.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import ru.vadimio.booklibrary.exceptions.ValidationException;

@ControllerAdvice
@RestController
public class ExceptionHandlerController {

  @ExceptionHandler(ValidationException.class)
  public final ResponseEntity<String> handleCommonException(Exception ex,
      WebRequest request) {
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    ResponseStatus responseStatus = ex.getClass().getAnnotation(ResponseStatus.class);
    if (responseStatus != null) {
      status = responseStatus.value();
    }
    return new ResponseEntity<>(ex.getMessage(), status);
  }
}
