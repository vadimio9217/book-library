package ru.vadimio.booklibrary.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.vadimio.booklibrary.exceptions.ValidationException;
import ru.vadimio.booklibrary.models.BookDto;
import ru.vadimio.booklibrary.services.BookService;
import ru.vadimio.booklibrary.services.ValidationService;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v1/books")
@Api("Endpoint for books")
public class BookController {

  private final BookService bookService;
  private final ValidationService validationService;

  @GetMapping
  @ApiOperation("Returns 20 books on every page by using filters.")
  public ResponseEntity<Page<BookDto>> getBooks(
      @RequestParam(name = "searchKey", required = false) String searchKey,
      @RequestParam(name = "year", required = false) Short year,
      @RequestParam(name = "authorsNum", required = false) Long authorsNum
  ) throws ValidationException {
    validationService.validate(searchKey, year, authorsNum);
    return ResponseEntity.ok(bookService.findByFilters(searchKey, year, authorsNum));
  }
}
