create table if not exists books (
    id uuid,
    title varchar(255) unique,
    year smallint,
    primary key (id)
);

create table if not exists authors (
    id uuid,
    name varchar(255) unique,
    book_id uuid,
    foreign key (book_id) references books (id),
    primary key (id)
);

insert into books(id, title, year) values
('72a6a5e6-9829-4100-a49b-a11bcee20571', 'The new war', 2020),
('8f7a45dd-c15c-4899-969b-4a5b098607d9', 'Harry Potter', null),
('a548f282-1683-4dd8-b3b0-8947824690ad', 'Martin Eden', 1909);

insert into authors(id, name, book_id) values
('2808a75e-09a2-423e-882b-a0cef1bc28d0', 'Jack London', 'a548f282-1683-4dd8-b3b0-8947824690ad'),
('160c5e35-a2b0-4c2d-b502-9ef495958d25', 'Иванов Иван Иванович', '72a6a5e6-9829-4100-a49b-a11bcee20571'),
('0d531a3c-1954-43aa-bf0d-5d286e99f212', 'Сидоров Николай Андреевич', '72a6a5e6-9829-4100-a49b-a11bcee20571');